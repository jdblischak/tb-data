# Mycobacterial infection induces a specific human innate immune response

This repo contains the summary data files for the following manuscript:

> Blischak, J. D. et al. Mycobacterial infection induces a specific human innate immune response. Sci. Rep. 5, 16882; doi: 10.1038/srep16882 (2015).

Other links related to this publication:

*  [Pre-print](http://biorxiv.org/content/early/2015/04/03/017483)
*  [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/26586179)
*  [Journal](http://www.nature.com/articles/srep16882)
*  [GEO](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE67427)
*  [Code repository](https://bitbucket.org/jdblischak/tb)
*  [Paper repository](https://bitbucket.org/jdblischak/tb-paper)

## Description

Supplementary Tables 1-9 are contained in the files that start with `table-sX`, where `X` is the number of the table you want.
The description of these tables is contained in the [Supplementary Information][si], which is also available via [Bitbucket][si-bitbucket].

[si]: http://www.nature.com/articles/srep16882#supplementary-information
[si-bitbucket]: https://bitbucket.org/jdblischak/tb-paper/src/master/supplementary-information.md

If you want to quickly explore this data, the best option is `table-s1.txt`.
It contains the batch-corrected log~2~ counts per million for the 12,728 Ensembl genes analyzed in this study for each of the 156 samples.
The column names are in the format "individual.infection.time".
If you'd prefer to start from the raw counts, use `counts_per_run.txt` (each row contains the counts from one lane of a flow cell for a given sample) or `counts_per_sample.txt` (each row contains the counts for a sample summed across all lanes).

Other useful files include:

*  `annotation.txt` - Contains the meta-data for the 156 samples, including experimental details and quality information (e.g. RIN).
*  `exons.txt` - Contains the exons used for calculating gene counts with [featureCounts][fc].
Note that this file contains duplicate exon entries, which are properly handled by [featureCounts][fc], but may be inappropriate input for other programs.

[fc]: http://bioinf.wehi.edu.au/featureCounts/

## License

This content is available via the [CC BY 3.0 license](https://creativecommons.org/licenses/by/3.0/).
