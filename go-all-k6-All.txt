GO.ID	Term	Annotated	Significant	Expected	weightFisher
GO:0006955	immune response	1026	154	61.01	6.6e-14
GO:0045944	positive regulation of transcription fro...	579	78	34.43	2.6e-11
GO:0071222	cellular response to lipopolysaccharide	100	30	5.95	1.7e-10
GO:0007267	cell-cell signaling	645	79	38.36	3.3e-10
GO:0051607	defense response to virus	163	38	9.69	1.2e-08
GO:0060337	type I interferon signaling pathway	64	18	3.81	1.1e-06
GO:0008285	negative regulation of cell proliferatio...	424	64	25.21	1.1e-06
GO:0071320	cellular response to cAMP	26	10	1.55	1.1e-06
GO:0048711	positive regulation of astrocyte differe...	9	6	0.54	3.1e-06
GO:0002675	positive regulation of acute inflammator...	20	8	1.19	3.2e-06
GO:0042832	defense response to protozoan	13	7	0.77	3.2e-06
GO:0072540	T-helper 17 cell lineage commitment	6	5	0.36	4.2e-06
GO:0070374	positive regulation of ERK1 and ERK2 cas...	67	15	3.98	6.7e-06
GO:0046641	positive regulation of alpha-beta T cell...	10	6	0.59	7.4e-06
GO:0045429	positive regulation of nitric oxide bios...	25	9	1.49	7.6e-06
GO:0042036	negative regulation of cytokine biosynth...	20	8	1.19	1.0e-05
GO:0042510	regulation of tyrosine phosphorylation o...	7	5	0.42	1.4e-05
GO:0042517	positive regulation of tyrosine phosphor...	16	7	0.95	1.8e-05
GO:0046888	negative regulation of hormone secretion	34	8	2.02	2.9e-05
GO:0050710	negative regulation of cytokine secretio...	23	8	1.37	3.3e-05
GO:0045071	negative regulation of viral genome repl...	37	10	2.2	4.1e-05
GO:0045655	regulation of monocyte differentiation	10	5	0.59	5.9e-05
GO:0060558	regulation of calcidiol 1-monooxygenase ...	5	4	0.3	5.9e-05
GO:0031652	positive regulation of heat generation	5	4	0.3	5.9e-05
GO:0048662	negative regulation of smooth muscle cel...	19	7	1.13	6.8e-05
GO:0060333	interferon-gamma-mediated signaling path...	69	16	4.1	7.0e-05
GO:0034105	positive regulation of tissue remodeling	20	6	1.19	7.5e-05
GO:0043011	myeloid dendritic cell differentiation	14	6	0.83	8.6e-05
GO:0046697	decidualization	14	6	0.83	8.6e-05
GO:0051384	response to glucocorticoid	81	21	4.82	9.9e-05
GO:0032735	positive regulation of interleukin-12 pr...	20	7	1.19	1.0e-04
GO:0006954	inflammatory response	402	72	23.91	0.00011
GO:0051591	response to cAMP	54	18	3.21	0.00015
GO:0048661	positive regulation of smooth muscle cel...	35	9	2.08	0.00015
GO:0001706	endoderm formation	36	6	2.14	0.00017
GO:0033089	positive regulation of T cell differenti...	6	4	0.36	0.00017
GO:0002676	regulation of chronic inflammatory respo...	6	4	0.36	0.00017
GO:0032693	negative regulation of interleukin-10 pr...	6	4	0.36	0.00017
GO:0019441	tryptophan catabolic process to kynureni...	6	4	0.36	0.00017
GO:2001236	regulation of extrinsic apoptotic signal...	127	17	7.55	0.00020
GO:0032653	regulation of interleukin-10 production	26	10	1.55	0.00020
GO:0031954	positive regulation of protein autophosp...	16	6	0.95	0.00021
GO:0045625	regulation of T-helper 1 cell differenti...	8	5	0.48	0.00021
GO:0032800	receptor biosynthetic process	19	4	1.13	0.00021
GO:0042102	positive regulation of T cell proliferat...	59	17	3.51	0.00022
GO:0043922	negative regulation by host of viral tra...	11	5	0.65	0.00025
GO:0044344	cellular response to fibroblast growth f...	147	13	8.74	0.00025
GO:0032496	response to lipopolysaccharide	174	43	10.35	0.00026
GO:0019722	calcium-mediated signaling	71	13	4.22	0.00026
GO:0071347	cellular response to interleukin-1	37	9	2.2	0.00027
GO:0051781	positive regulation of cell division	49	9	2.91	0.00027
GO:0045582	positive regulation of T cell differenti...	45	15	2.68	0.00029
GO:0043066	negative regulation of apoptotic process	500	56	29.73	0.00029
GO:0009615	response to virus	242	52	14.39	0.00031
GO:0032480	negative regulation of type I interferon...	37	9	2.2	0.00035
GO:1900745	positive regulation of p38MAPK cascade	7	4	0.42	0.00038
GO:0032645	regulation of granulocyte macrophage col...	7	4	0.42	0.00038
GO:0030728	ovulation	12	5	0.71	0.00041
GO:0032729	positive regulation of interferon-gamma ...	32	8	1.9	0.00044
GO:0000188	inactivation of MAPK activity	25	7	1.49	0.00048
GO:0030154	cell differentiation	2115	205	125.77	0.00050
GO:0050728	negative regulation of inflammatory resp...	61	13	3.63	0.00050
GO:0032868	response to insulin	198	15	11.77	0.00057
GO:0002526	acute inflammatory response	79	23	4.7	0.00057
GO:0050873	brown fat cell differentiation	19	6	1.13	0.00060
GO:0060397	JAK-STAT cascade involved in growth horm...	19	6	1.13	0.00060
GO:0010755	regulation of plasminogen activation	8	4	0.48	0.00072
GO:0006700	C21-steroid hormone biosynthetic process	8	4	0.48	0.00072
GO:2000833	positive regulation of steroid hormone s...	8	4	0.48	0.00072
GO:0042542	response to hydrogen peroxide	82	14	4.88	0.00077
GO:0031663	lipopolysaccharide-mediated signaling pa...	44	11	2.62	0.00077
GO:0002768	immune response-regulating cell surface ...	266	25	15.82	0.00078
GO:0045622	regulation of T-helper cell differentiat...	17	9	1.01	0.00078
GO:0034121	regulation of toll-like receptor signali...	28	7	1.67	0.00079
GO:0034114	regulation of heterotypic cell-cell adhe...	9	5	0.54	0.00079
GO:0034097	response to cytokine	473	80	28.13	0.00080
GO:0032715	negative regulation of interleukin-6 pro...	20	6	1.19	0.00082
GO:0007186	G-protein coupled receptor signaling pat...	375	38	22.3	0.00086
GO:0060326	cell chemotaxis	160	29	9.51	0.00089
GO:0001960	negative regulation of cytokine-mediated...	24	6	1.43	0.00093
GO:0002230	positive regulation of defense response ...	14	5	0.83	0.00093
GO:0034138	toll-like receptor 3 signaling pathway	82	13	4.88	0.00103
GO:0007155	cell adhesion	635	60	37.76	0.00112
GO:0070233	negative regulation of T cell apoptotic ...	14	6	0.83	0.00122
GO:0035924	cellular response to vascular endothelia...	21	5	1.25	0.00123
GO:0060707	trophoblast giant cell differentiation	9	4	0.54	0.00123
GO:0032355	response to estradiol	56	12	3.33	0.00130
GO:0051412	response to corticosterone	15	5	0.89	0.00133
GO:0035666	TRIF-dependent toll-like receptor signal...	75	12	4.46	0.00146
GO:0043434	response to peptide hormone	284	27	16.89	0.00149
GO:0010628	positive regulation of gene expression	894	101	53.16	0.00151
GO:0007169	transmembrane receptor protein tyrosine ...	522	49	31.04	0.00163
GO:0006874	cellular calcium ion homeostasis	182	20	10.82	0.00172
GO:0043491	protein kinase B signaling	97	15	5.77	0.00178
GO:2001240	negative regulation of extrinsic apoptot...	23	6	1.37	0.00182
GO:0022409	positive regulation of cell-cell adhesio...	23	6	1.37	0.00182
GO:0045576	mast cell activation	34	6	2.02	0.00190
GO:0032755	positive regulation of interleukin-6 pro...	31	7	1.84	0.00191
GO:2001212	regulation of vasculogenesis	5	3	0.3	0.00191
GO:0035907	dorsal aorta development	5	3	0.3	0.00191
GO:0097084	vascular smooth muscle cell development	5	3	0.3	0.00191
GO:0010533	regulation of activation of Janus kinase...	5	3	0.3	0.00191
GO:0002439	chronic inflammatory response to antigen...	5	3	0.3	0.00191
GO:0001780	neutrophil homeostasis	5	3	0.3	0.00191
GO:0032819	positive regulation of natural killer ce...	5	3	0.3	0.00191
GO:0001866	NK T cell proliferation	5	3	0.3	0.00191
GO:0007262	STAT protein import into nucleus	5	3	0.3	0.00191
GO:0031620	regulation of fever generation	5	3	0.3	0.00191
GO:0035771	interleukin-4-mediated signaling pathway	5	3	0.3	0.00191
GO:2000501	regulation of natural killer cell chemot...	5	3	0.3	0.00191
GO:0045650	negative regulation of macrophage differ...	5	3	0.3	0.00191
GO:0019221	cytokine-mediated signaling pathway	307	56	18.26	0.00191
GO:0010888	negative regulation of lipid storage	15	5	0.89	0.00194
GO:0043410	positive regulation of MAPK cascade	250	34	14.87	0.00205
GO:0051092	positive regulation of NF-kappaB transcr...	110	15	6.54	0.00212
GO:0043401	steroid hormone mediated signaling pathw...	32	7	1.9	0.00232
GO:0050679	positive regulation of epithelial cell p...	85	16	5.05	0.00236
GO:0030183	B cell differentiation	67	13	3.98	0.00241
GO:0043065	positive regulation of apoptotic process	313	38	18.61	0.00247
GO:0006469	negative regulation of protein kinase ac...	161	23	9.57	0.00261
GO:0034142	toll-like receptor 4 signaling pathway	100	14	5.95	0.00269
GO:0008630	intrinsic apoptotic signaling pathway in...	98	15	5.83	0.00282
GO:0032722	positive regulation of chemokine product...	30	8	1.78	0.00285
GO:0002755	MyD88-dependent toll-like receptor signa...	81	12	4.82	0.00286
GO:0009409	response to cold	25	6	1.49	0.00289
GO:0030330	DNA damage response, signal transduction...	103	10	6.13	0.00291
GO:0045086	positive regulation of interleukin-2 bio...	11	4	0.65	0.00292
GO:0021537	telencephalon development	114	10	6.78	0.00292
GO:0006952	defense response	1048	159	62.32	0.00293
GO:0006953	acute-phase response	25	10	1.49	0.00318
GO:0050715	positive regulation of cytokine secretio...	52	11	3.09	0.00348
GO:0030195	negative regulation of blood coagulation	24	8	1.43	0.00348
GO:0001660	fever generation	7	5	0.42	0.00350
GO:0045072	regulation of interferon-gamma biosynthe...	12	5	0.71	0.00351
GO:0055074	calcium ion homeostasis	188	23	11.18	0.00351
GO:1903115	regulation of actin filament-based movem...	22	5	1.31	0.00351
GO:0061097	regulation of protein tyrosine kinase ac...	40	6	2.38	0.00351
GO:0030730	sequestering of triglyceride	11	3	0.65	0.00353
GO:0009746	response to hexose	91	7	5.41	0.00353
GO:0042130	negative regulation of T cell proliferat...	32	7	1.9	0.00355
GO:0045840	positive regulation of mitosis	26	6	1.55	0.00357
GO:0050731	positive regulation of peptidyl-tyrosine...	85	20	5.05	0.00361
GO:0007165	signal transduction	3434	318	204.21	0.00363
GO:0032486	Rap protein signal transduction	19	5	1.13	0.00364
GO:0051023	regulation of immunoglobulin secretion	6	3	0.36	0.00366
GO:0030853	negative regulation of granulocyte diffe...	6	3	0.36	0.00366
GO:0060693	regulation of branching involved in sali...	6	3	0.36	0.00366
GO:0060670	branching involved in labyrinthine layer...	6	3	0.36	0.00366
GO:0002739	regulation of cytokine secretion involve...	6	3	0.36	0.00366
GO:0042523	positive regulation of tyrosine phosphor...	6	3	0.36	0.00366
GO:0048505	regulation of timing of cell differentia...	6	3	0.36	0.00366
GO:0051953	negative regulation of amine transport	6	3	0.36	0.00366
GO:0042977	activation of JAK2 kinase activity	6	3	0.36	0.00366
GO:2000668	regulation of dendritic cell apoptotic p...	6	3	0.36	0.00366
GO:0045820	negative regulation of glycolytic proces...	6	3	0.36	0.00366
GO:0051897	positive regulation of protein kinase B ...	53	9	3.15	0.00370
GO:0045766	positive regulation of angiogenesis	69	12	4.1	0.00370
GO:0001938	positive regulation of endothelial cell ...	44	8	2.62	0.00395
GO:0051353	positive regulation of oxidoreductase ac...	28	7	1.67	0.00413
GO:0090023	positive regulation of neutrophil chemot...	12	4	0.71	0.00418
GO:0045746	negative regulation of Notch signaling p...	12	4	0.71	0.00418
GO:0051926	negative regulation of calcium ion trans...	12	4	0.71	0.00418
GO:0032088	negative regulation of NF-kappaB transcr...	54	9	3.21	0.00421
GO:0042493	response to drug	257	29	15.28	0.00456
GO:0045740	positive regulation of DNA replication	45	8	2.68	0.00466
GO:0007166	cell surface receptor signaling pathway	1879	190	111.74	0.00472
GO:0007565	female pregnancy	111	17	6.6	0.00499
GO:0001889	liver development	82	13	4.88	0.00519
GO:0001525	angiogenesis	258	36	15.34	0.00561
GO:0002690	positive regulation of leukocyte chemota...	42	10	2.5	0.00562
GO:0031327	negative regulation of cellular biosynth...	841	70	50.01	0.00563
GO:0060749	mammary gland alveolus development	13	4	0.77	0.00575
GO:0071636	positive regulation of transforming grow...	7	3	0.42	0.00612
GO:0031665	negative regulation of lipopolysaccharid...	7	3	0.42	0.00612
GO:0000185	activation of MAPKKK activity	7	3	0.42	0.00612
GO:0071356	cellular response to tumor necrosis fact...	79	12	4.7	0.00669
GO:0032728	positive regulation of interferon-beta p...	21	5	1.25	0.00672
GO:0006959	humoral immune response	87	15	5.17	0.00722
GO:0048754	branching morphogenesis of an epithelial...	99	16	5.89	0.00739
GO:0009612	response to mechanical stimulus	122	17	7.26	0.00742
GO:0045987	positive regulation of smooth muscle con...	14	4	0.83	0.00768
GO:0035066	positive regulation of histone acetylati...	14	4	0.83	0.00768
GO:0033138	positive regulation of peptidyl-serine p...	49	8	2.91	0.00774
GO:0045444	fat cell differentiation	116	23	6.9	0.00775
GO:0050729	positive regulation of inflammatory resp...	61	15	3.63	0.00806
GO:0014070	response to organic cyclic compound	451	76	26.82	0.00813
GO:0042346	positive regulation of NF-kappaB import ...	22	5	1.31	0.00827
GO:0046887	positive regulation of hormone secretion	51	9	3.03	0.00922
GO:2000108	positive regulation of leukocyte apoptot...	22	5	1.31	0.00931
GO:0071350	cellular response to interleukin-15	8	3	0.48	0.00936
GO:0010544	negative regulation of platelet activati...	8	3	0.48	0.00936
GO:0032740	positive regulation of interleukin-17 pr...	8	3	0.48	0.00936
GO:0002827	positive regulation of T-helper 1 type i...	8	3	0.48	0.00936
GO:0043501	skeletal muscle adaptation	8	3	0.48	0.00936
GO:0014912	negative regulation of smooth muscle cel...	8	3	0.48	0.00936
GO:0060395	SMAD protein signal transduction	8	3	0.48	0.00936
GO:0010460	positive regulation of heart rate	8	3	0.48	0.00936
GO:0070102	interleukin-6-mediated signaling pathway	8	3	0.48	0.00936
GO:0002819	regulation of adaptive immune response	89	21	5.29	0.00975
GO:0032680	regulation of tumor necrosis factor prod...	64	12	3.81	0.00998
GO:0042104	positive regulation of activated T cell ...	15	4	0.89	0.00999
GO:0019953	sexual reproduction	391	31	23.25	0.00999
