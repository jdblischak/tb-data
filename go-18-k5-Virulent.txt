GO.ID	Term	Annotated	Significant	Expected	weightFisher
GO:0042273	ribosomal large subunit biogenesis	16	7	1.44	0.00026
GO:0007339	binding of sperm to zona pellucida	18	7	1.62	0.00062
GO:0006413	translational initiation	168	28	15.15	0.00116
GO:0022904	respiratory electron transport chain	102	19	9.2	0.00128
GO:0044267	cellular protein metabolic process	3111	352	280.63	0.00142
GO:0043484	regulation of RNA splicing	60	12	5.41	0.00190
GO:0000387	spliceosomal snRNP assembly	33	9	2.98	0.00201
GO:0034660	ncRNA metabolic process	319	58	28.78	0.00209
GO:0006385	transcription elongation from RNA polyme...	17	6	1.53	0.00275
GO:0006386	termination of RNA polymerase III transc...	17	6	1.53	0.00275
GO:0006414	translational elongation	119	22	10.73	0.00292
GO:0006614	SRP-dependent cotranslational protein ta...	107	19	9.65	0.00307
GO:0031124	mRNA 3'-end processing	93	13	8.39	0.00315
GO:0006449	regulation of translational termination	8	4	0.72	0.00342
GO:0006626	protein targeting to mitochondrion	56	12	5.05	0.00364
GO:0006415	translational termination	95	20	8.57	0.00429
GO:0006417	regulation of translation	236	44	21.29	0.00473
GO:0008334	histone mRNA metabolic process	24	8	2.16	0.00587
GO:0060840	artery development	36	6	3.25	0.00636
GO:0042373	vitamin K metabolic process	5	3	0.45	0.00637
GO:0006489	dolichyl diphosphate biosynthetic proces...	5	3	0.45	0.00637
GO:0006108	malate metabolic process	5	3	0.45	0.00637
GO:0071910	determination of liver left/right asymme...	5	3	0.45	0.00637
GO:0070166	enamel mineralization	5	3	0.45	0.00637
GO:0006099	tricarboxylic acid cycle	26	7	2.35	0.00678
GO:0006418	tRNA aminoacylation for protein translat...	46	10	4.15	0.00689
GO:0019083	viral transcription	158	23	14.25	0.00752
GO:0018279	protein N-linked glycosylation via aspar...	92	16	8.3	0.00772
GO:0051057	positive regulation of small GTPase medi...	24	5	2.16	0.00811
GO:0035116	embryonic hindlimb morphogenesis	15	5	1.35	0.00823
GO:0000184	nuclear-transcribed mRNA catabolic proce...	118	19	10.64	0.00909
